package com.zooker.customer;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.zooker.RegistryService;

@RequestMapping("/sendMoney")
@Controller
@Scope("prototype")	
public class CustomerRegistryController {
	@Autowired
	RegistryService registryService;
	
	@RequestMapping("/createCustomer")
	public ModelAndView createCustomer(HttpServletRequest request, HttpServletResponse response){
		ModelAndView modelAndView = new ModelAndView();
		String result = registryService.hello("asf");
		System.out.println(result);
		return modelAndView;
	}
}
